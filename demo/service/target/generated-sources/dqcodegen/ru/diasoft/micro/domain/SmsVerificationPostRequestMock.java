/*
 * Created by DQCodegen
 */
package ru.diasoft.micro.domain;

import lombok.Generated;

@Generated
public class SmsVerificationPostRequestMock extends SmsVerificationPostRequest {

    private static final long serialVersionUID = 1L;
    
    public SmsVerificationPostRequestMock() {
        super(
            "PhoneNumber"
        );
    }

}
