package ru.diasoft.micro.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "sms_verification")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SmsVerification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "process_guid")
    private String processGuid;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "secret_code")
    private String secretCode;
}
